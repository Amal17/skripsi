<?php

class Model extends CI_Model
{
	function get_kelas_pelajaran()
    {
        // $sql = "SELECT kp.id_kelas, k.kelas, CONCAT( '[', GROUP_CONCAT(
        //             CONCAT('{\"id_pelajaran\":\"', p.id_pelajaran, '\", \"pelajaran\":\"',p.pelajaran,'\", \"jam_pelajaran\":\"',kp.jam_pelajaran,'\"}')
        //         ), ']') AS kelas_pelajaran
        //         FROM kelas_pelajaran kp, pelajaran p, kelas k
        //         WHERE kp.id_pelajaran = p.id_pelajaran AND kp.id_kelas = k.id_kelas
        //         GROUP BY kp.id_kelas";

        $sql = "SELECT kp.id_kelas, k.kelas, CONCAT( '[', GROUP_CONCAT(
                    CONCAT('{\"id_pelajaran\":\"', p.id_pelajaran, '\", \"pelajaran\":\"',p.kode,'\", \"jam_pelajaran\":\"',kp.jam_pelajaran,'\"}')
                ), ']') AS kelas_pelajaran
                FROM kelas_pelajaran kp, pelajaran p, kelas k
                WHERE kp.id_pelajaran = p.id_pelajaran AND kp.id_kelas = k.id_kelas
                GROUP BY kp.id_kelas";

        $r = $this->db->query($sql)->result_array();

        foreach ($r as $key => $value) {
            $r[$key]['kelas_pelajaran'] = json_decode($value['kelas_pelajaran']);
        }
        
        return $r;
    }
    
    function get_guru_pelajaran()
    {
        // $sql = "SELECT gp.id_pelajaran, p.pelajaran, CONCAT( '[', GROUP_CONCAT(
        //             CONCAT('{\"id_guru\":\"', g.id_guru, '\", \"guru\":\"',g.guru,'\", \"jam_mengajar\":\"',gp.jam_mengajar,'\"}')
        //         ), ']') AS guru_pelajaran
        //         FROM guru_pelajaran gp, pelajaran p, guru g
        //         WHERE gp.id_pelajaran = p.id_pelajaran AND gp.id_guru = g.id_guru
        //         GROUP BY gp.id_pelajaran";

        $sql = "SELECT gp.id_pelajaran, p.kode as pelajaran, CONCAT( '[', GROUP_CONCAT(
            CONCAT('{\"id_guru\":\"', g.id_guru, '\", \"guru\":\"',g.kode_guru,'\", \"jam_mengajar\":\"',gp.jam_mengajar,'\"}')
        ), ']') AS guru_pelajaran
        FROM guru_pelajaran gp, pelajaran p, guru g
        WHERE gp.id_pelajaran = p.id_pelajaran AND gp.id_guru = g.id_guru
        GROUP BY gp.id_pelajaran";

        $r = $this->db->query($sql)->result_array();

        foreach ($r as $key => $value) {
			$r[$key]['guru_pelajaran'] = json_decode($value['guru_pelajaran']);
        }
        
        return $r;
    }
    
    function get_jam_perhari()
    {
        return $this->db->get('jam')->row();
    }

    function get_libur_guru()
    {
        return $this->db->get('libur')->result_array();
    }

    function grafik($skenario=''){

        $where = $skenario == null ? '' : "WHERE skenario = '".$skenario."'";
       $sql = "SELECT 
                    skenario,
                    CONCAT('[', GROUP_CONCAT(tabrak) ,']') AS tabrak, 
                    CONCAT('[', GROUP_CONCAT(libur) ,']') AS libur,
                    CONCAT('[', GROUP_CONCAT(guru) ,']') AS guru,
                    CONCAT('[', GROUP_CONCAT(pelajaran) ,']') AS pelajaran,
                    CONCAT('[', GROUP_CONCAT(fitness/10000) ,']') AS fitness,
                    CONCAT('[', GROUP_CONCAT(generasi_ke) ,']') AS generasi_ke
                FROM hasil_per_generasi ".$where."
                    GROUP BY skenario ORDER BY generasi_ke";
        
        return $this->db->query($sql)->result_array();
    }
}