  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Master Guru Pelajaran</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Guru Pelajaran</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-6">

        <div class="card card-warning">
              <div class="card-header">
                <h3 class="card-title">Edit Guru Pelajaran</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form role="form" action="<?=base_url('/master/updateGuruPelajaran')?>" method="POST">
                <div class="card-body">
                  <div class="form-group">
                    <label for="exampleInputEmail1">Guru</label>
                    <input type="text" class="form-control" readonly value="<?=$data->guru?>">
                    <input type="hidden" class="form-control" readonly name="id_guru_pelajaran" value="<?=$data->id_guru_pelajaran?>">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputPassword1">Pelajaran</label>
                    <input type="text" class="form-control" readonly value="<?=$data->pelajaran?>">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputPassword1">Jam Mengajar</label>
                    <input type="text" class="form-control" name="jam_mengajar" value="<?=$data->jam_mengajar?>">
                  </div>
                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-warning">Submit</button>
                </div>
              </form>
            </div>
            </div>
        </div>
        </div>
        
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->