<html>
  <head>
    <meta charset="utf-8">
    <title>Demo Grafik Garis</title>
    <script src="<?=base_url().'assets/chartjs/Chart.js'?>"></script>
    <style type="text/css">
            .container {
                /* width: 50%; */
                /* margin: 5px; */
                display:grid;
                grid-template-columns: 80% auto;
                /* padding: 4em 1em; */
            }
            ul {
                display:grid;
                list-style-type:none;
                margin:0;padding:0;
                grid-template-columns: repeat(2, auto);
                /* grid-template-rows: repeat(2, auto); */
            }
    </style>
  </head>
  <body>

    <?php 
      foreach ($grafik as $data) {
    ?>    
    
    <div class="container">
        <div><canvas id="<?=$data['skenario']?>"></canvas></div>
    </div>
    <div class="container">
        <div><canvas id="<?=$data['skenario'].'2'?>"></canvas></div>
    </div>

      	<script  type="text/javascript">

        var ctx = document.getElementById("<?=$data['skenario']?>").getContext("2d");
    	  var ctx2 = document.getElementById("<?=$data['skenario'].'2'?>").getContext("2d");
    	  var data = {
    	            labels: <?=$data['generasi_ke']?>,
    	            datasets: [
    	            {
    	              label: "Tabrak",
                    fill: false,
                    lineTension: 0.1,
                    backgroundColor: "rgba(59, 100, 222, 1)",
                    borderColor: "rgba(59, 100, 222, 1)",
                    pointHoverBackgroundColor: "rgba(59, 100, 222, 1)",
						        pointHoverBorderColor: "rgba(59, 100, 222, 1)",
                        data: <?=$data['tabrak']?>
    	            },
                  {
    	              label: "Libur",
                    fill: false,
                    lineTension: 0.1,
                    backgroundColor: "rgba(59, 100, 222, 1)",
                    borderColor: "rgba(59, 100, 222, 1)",
                    pointHoverBackgroundColor: "rgba(59, 100, 222, 1)",
						        pointHoverBorderColor: "rgba(59, 100, 222, 1)",
                        data: <?=$data['libur']?>
    	            },
                  {
    	              label: "Guru",
                    fill: false,
                    lineTension: 0.1,
                    backgroundColor: "rgba(203, 222, 225, 0.9)",
                    borderColor: "rgba(203, 222, 225, 0.9)",
                    pointHoverBackgroundColor: "rgba(203, 222, 225, 0.9)",
						        pointHoverBorderColor: "rgba(203, 222, 225, 0.9)",
                        data: <?=$data['guru']?>
    	            },
                  {
    	              label: "Pelajaran",
                    fill: false,
                    lineTension: 0.1,
                    backgroundColor: "rgba(203, 222, 225, 0.9)",
                    borderColor: "rgba(203, 222, 225, 0.9)",
                    pointHoverBackgroundColor: "rgba(203, 222, 225, 0.9)",
						        pointHoverBorderColor: "rgba(203, 222, 225, 0.9)",
                        data: <?=$data['pelajaran']?>
    	            },
    	            ]
    	            };

    	  var myBarChart = new Chart(ctx, {
    	            type: 'line',
    	            data: data,
    	            options: {
    	            barValueSpacing: 20,
    	            scales: {
    	              yAxes: [{
    	                  ticks: {
                            beginAtZero: true,
    	                      min: 0,
    	                  }
    	              }],
    	              xAxes: [{
    	                          gridLines: {
    	                              color: "rgba(0, 0, 0, 0)",
    	                          }
    	                      }]
    	              },
                    elements: { 
                      point: { 
                        radius: 0,
                        hitRadius: 10, 
                        hoverRadius: 5,
                      } 
                    }
    	          }
    	        });

        var data2 = {
    	            labels: <?=$data['generasi_ke']?>,
    	            datasets: [
    	            {
    	              label: "Fitness",
                    fill: false,
                    lineTension: 0.1,
                    backgroundColor: "rgba(59, 100, 222, 1)",
                    borderColor: "rgba(59, 100, 222, 1)",
                    pointHoverBackgroundColor: "rgba(59, 100, 222, 1)",
						        pointHoverBorderColor: "rgba(59, 100, 222, 1)",
                        data: <?=$data['fitness']?>
    	            }
    	            ]
                };

        var myBarChart = new Chart(ctx2, {
    	            type: 'line',
    	            data: data2,
    	            options: {
    	            barValueSpacing: 20,
    	            scales: {
    	              yAxes: [{
    	                  ticks: {
                            beginAtZero: true,
    	                      min: 0,
    	                  }
    	              }],
    	              xAxes: [{
    	                          gridLines: {
    	                              color: "rgba(0, 0, 0, 0)",
    	                          }
    	                      }]
    	              },
                    elements: { 
                      point: { 
                        radius: 0,
                        hitRadius: 10, 
                        hoverRadius: 5,
                      } 
                    }
    	          }
    	        });
    	</script>
      <?php  } ?>
  </body>
</html>