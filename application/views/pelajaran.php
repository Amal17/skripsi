  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Master Pelajaran</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Pelajaran</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-6">

        <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Tambah Pelajaran</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form role="form" action="<?=base_url('/master/addPelajaran')?>" method="POST">
                <div class="card-body">
                  <div class="form-group">
                    <label for="exampleInputEmail1">Nama Guru</label>
                    <input type="text" class="form-control" name="pelajaran" placeholder="Pelajaran">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputPassword1">Kode Guru</label>
                    <input type="text" class="form-control" name="kode" placeholder="KODE">
                  </div>
                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Submit</button>
                </div>
              </form>
            </div>
            </div>
          
            <!-- <div class="row"> -->
          <div class="col-6">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">Daftar Pelajaran</h3>

                <div class="card-tools">
                  <div class="input-group input-group-sm" style="width: 100px;">
                    <!-- <input type="text" name="table_search" class="form-control float-right" placeholder="Search"> -->

                    <div class="input-group-append">
                      <!-- <button type="submit" class="btn btn-default"><i class="fas fa-search"></i></button> -->
                    </div>
                  </div>
                </div>
              </div>
              <!-- /.card-header -->
              <div class="card-body table-responsive p-0" style="height: 300px;">
                <table class="table table-head-fixed text-nowrap">
                  <thead>
                    <tr>
                      <th>ID</th>
                      <th>Pelajaran</th>
                      <th>Kode</th>
                      <th>Aksi</th>
                      <th></th>
                    </tr>
                  </thead>
                  <tbody>
                      <?php
                      foreach ($data as $v){
                        echo '<tr><td>'.$v['id_pelajaran'].'</td><td>'.$v['pelajaran'].'</td><td>'.$v['kode'].'</td>';
                        echo '<td><span class="badge bg-warning"><a href="'.base_url('master/editPelajaran/').$v['id_pelajaran'].'">EDIT</a></span></td>';
                        echo '<td><span class="badge bg-danger"><a href="'.base_url('master/delPelajaran/').$v['id_pelajaran'].'">HAPUS</a></span></td>';
                        echo '</tr>';
                      }
                      ?>
                  </tbody>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
        </div>
            <!-- /.col-md-6 -->
        </div>
        <!-- /.row -->
        
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->