-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 12, 2020 at 06:00 AM
-- Server version: 10.4.14-MariaDB
-- PHP Version: 7.4.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `penjadwalan1`
--

-- --------------------------------------------------------

--
-- Table structure for table `guru`
--

CREATE TABLE `guru` (
  `id_guru` int(11) NOT NULL,
  `guru` varchar(50) NOT NULL,
  `kode_guru` varchar(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `guru`
--

INSERT INTO `guru` (`id_guru`, `guru`, `kode_guru`) VALUES
(1, 'Abdul', 'G1'),
(2, 'Bedol', 'G2'),
(3, 'Cherlie', 'G3');

-- --------------------------------------------------------

--
-- Table structure for table `guru_pelajaran`
--

CREATE TABLE `guru_pelajaran` (
  `id_guru_pelajaran` int(11) NOT NULL,
  `id_guru` int(11) NOT NULL,
  `id_pelajaran` int(11) NOT NULL,
  `jam_mengajar` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `guru_pelajaran`
--

INSERT INTO `guru_pelajaran` (`id_guru_pelajaran`, `id_guru`, `id_pelajaran`, `jam_mengajar`) VALUES
(1, 1, 1, 15),
(2, 1, 2, 5),
(4, 2, 2, 10),
(5, 2, 3, 4),
(7, 2, 4, 4),
(8, 2, 5, 6),
(9, 2, 6, 6),
(10, 2, 7, 4),
(11, 2, 8, 4),
(12, 2, 9, 4),
(13, 2, 10, 4),
(14, 2, 11, 4),
(15, 3, 3, 2),
(16, 3, 4, 2),
(17, 3, 5, 3),
(18, 3, 6, 3),
(19, 3, 7, 2),
(20, 3, 8, 2),
(21, 3, 9, 2),
(22, 3, 10, 2),
(24, 3, 11, 2);

-- --------------------------------------------------------

--
-- Table structure for table `jam`
--

CREATE TABLE `jam` (
  `id_jam` int(11) NOT NULL,
  `senin` int(11) NOT NULL,
  `selasa` int(11) NOT NULL,
  `rabu` int(11) NOT NULL,
  `kamis` int(11) NOT NULL,
  `jumat` int(11) NOT NULL,
  `sabtu` int(11) NOT NULL,
  `minggu` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jam`
--

INSERT INTO `jam` (`id_jam`, `senin`, `selasa`, `rabu`, `kamis`, `jumat`, `sabtu`, `minggu`) VALUES
(1, 5, 7, 7, 6, 5, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `kelas`
--

CREATE TABLE `kelas` (
  `id_kelas` int(11) NOT NULL,
  `kelas` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kelas`
--

INSERT INTO `kelas` (`id_kelas`, `kelas`) VALUES
(1, '1 A'),
(2, '2 A'),
(3, '3 A');

-- --------------------------------------------------------

--
-- Table structure for table `kelas_pelajaran`
--

CREATE TABLE `kelas_pelajaran` (
  `id_kelas_pelajaran` int(11) NOT NULL,
  `id_kelas` int(11) NOT NULL,
  `id_pelajaran` int(11) NOT NULL,
  `jam_pelajaran` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kelas_pelajaran`
--

INSERT INTO `kelas_pelajaran` (`id_kelas_pelajaran`, `id_kelas`, `id_pelajaran`, `jam_pelajaran`) VALUES
(2, 1, 1, 5),
(3, 1, 2, 5),
(5, 1, 3, 2),
(6, 1, 4, 2),
(7, 1, 5, 3),
(8, 1, 6, 3),
(9, 1, 7, 2),
(10, 1, 8, 2),
(11, 1, 9, 2),
(12, 1, 10, 2),
(13, 1, 11, 2),
(14, 2, 1, 5),
(15, 2, 2, 5),
(16, 2, 3, 2),
(17, 2, 4, 2),
(18, 2, 5, 3),
(19, 2, 6, 3),
(20, 2, 7, 2),
(21, 2, 8, 2),
(22, 2, 9, 2),
(23, 2, 10, 2),
(24, 2, 11, 2),
(25, 3, 1, 5),
(26, 3, 2, 5),
(28, 3, 3, 2),
(29, 3, 4, 2),
(30, 3, 5, 3),
(31, 3, 6, 3),
(32, 3, 7, 2),
(33, 3, 8, 2),
(34, 3, 9, 2),
(35, 3, 10, 2),
(36, 3, 11, 2);

-- --------------------------------------------------------

--
-- Table structure for table `libur`
--

CREATE TABLE `libur` (
  `id_libur` int(11) NOT NULL,
  `id_guru` int(11) NOT NULL,
  `senin` tinyint(1) NOT NULL DEFAULT 0,
  `selasa` tinyint(1) NOT NULL DEFAULT 0,
  `rabu` tinyint(1) NOT NULL DEFAULT 0,
  `kamis` tinyint(1) NOT NULL DEFAULT 0,
  `jumat` tinyint(1) NOT NULL DEFAULT 0,
  `sabtu` tinyint(1) NOT NULL DEFAULT 0,
  `minggu` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `libur`
--

INSERT INTO `libur` (`id_libur`, `id_guru`, `senin`, `selasa`, `rabu`, `kamis`, `jumat`, `sabtu`, `minggu`) VALUES
(1, 1, 0, 0, 0, 0, 1, 0, 0),
(2, 2, 0, 0, 0, 1, 0, 0, 0),
(3, 3, 0, 0, 1, 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `pelajaran`
--

CREATE TABLE `pelajaran` (
  `id_pelajaran` int(11) NOT NULL,
  `pelajaran` varchar(50) NOT NULL,
  `kode` varchar(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pelajaran`
--

INSERT INTO `pelajaran` (`id_pelajaran`, `pelajaran`, `kode`) VALUES
(1, 'Matematika', 'MTK'),
(2, 'Bahasa Indonesia', 'BI'),
(3, 'Agama', 'AGM'),
(4, 'PKN', 'PKN'),
(5, 'Ilmu Pengetahuan Alam', 'IPA'),
(6, 'Ilmu Pengetahuan Sosial', 'IPS'),
(7, 'Bahasa Inggris', 'ENG'),
(8, 'Seni Budaya', 'SBD'),
(9, 'Olah Raga', 'ORG'),
(10, 'Prakarya', 'PKR'),
(11, 'Teknologi Informasi Komputer', 'TIK');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `guru`
--
ALTER TABLE `guru`
  ADD PRIMARY KEY (`id_guru`),
  ADD UNIQUE KEY `kode_guru` (`kode_guru`);

--
-- Indexes for table `guru_pelajaran`
--
ALTER TABLE `guru_pelajaran`
  ADD PRIMARY KEY (`id_guru_pelajaran`),
  ADD KEY `id_guru` (`id_guru`),
  ADD KEY `id_pelajaran` (`id_pelajaran`);

--
-- Indexes for table `jam`
--
ALTER TABLE `jam`
  ADD PRIMARY KEY (`id_jam`);

--
-- Indexes for table `kelas`
--
ALTER TABLE `kelas`
  ADD PRIMARY KEY (`id_kelas`);

--
-- Indexes for table `kelas_pelajaran`
--
ALTER TABLE `kelas_pelajaran`
  ADD PRIMARY KEY (`id_kelas_pelajaran`),
  ADD KEY `id_kelas` (`id_kelas`),
  ADD KEY `id_pelajaran` (`id_pelajaran`);

--
-- Indexes for table `libur`
--
ALTER TABLE `libur`
  ADD PRIMARY KEY (`id_libur`);

--
-- Indexes for table `pelajaran`
--
ALTER TABLE `pelajaran`
  ADD PRIMARY KEY (`id_pelajaran`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `guru`
--
ALTER TABLE `guru`
  MODIFY `id_guru` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `guru_pelajaran`
--
ALTER TABLE `guru_pelajaran`
  MODIFY `id_guru_pelajaran` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `jam`
--
ALTER TABLE `jam`
  MODIFY `id_jam` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `kelas`
--
ALTER TABLE `kelas`
  MODIFY `id_kelas` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `kelas_pelajaran`
--
ALTER TABLE `kelas_pelajaran`
  MODIFY `id_kelas_pelajaran` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;

--
-- AUTO_INCREMENT for table `libur`
--
ALTER TABLE `libur`
  MODIFY `id_libur` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `pelajaran`
--
ALTER TABLE `pelajaran`
  MODIFY `id_pelajaran` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `guru_pelajaran`
--
ALTER TABLE `guru_pelajaran`
  ADD CONSTRAINT `guru_pelajaran_ibfk_1` FOREIGN KEY (`id_guru`) REFERENCES `guru` (`id_guru`),
  ADD CONSTRAINT `guru_pelajaran_ibfk_2` FOREIGN KEY (`id_pelajaran`) REFERENCES `pelajaran` (`id_pelajaran`);

--
-- Constraints for table `kelas_pelajaran`
--
ALTER TABLE `kelas_pelajaran`
  ADD CONSTRAINT `kelas_pelajaran_ibfk_1` FOREIGN KEY (`id_kelas`) REFERENCES `kelas` (`id_kelas`),
  ADD CONSTRAINT `kelas_pelajaran_ibfk_2` FOREIGN KEY (`id_pelajaran`) REFERENCES `pelajaran` (`id_pelajaran`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
