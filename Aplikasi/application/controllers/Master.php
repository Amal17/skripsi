<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Master extends CI_Controller {

	function __construct() {
		parent::__construct();
		$this->load->model('master_model');
    }

    public function index(){
        redirect('master/guru');
    }

    public function guru()
    {
        $guru = $this->db->get('guru')->result_array();
        $this->load->view('header');
        $this->load->view('guru', ['guru' => $guru]);
        $this->load->view('footer');
    }

    public function addGuru()
    {
        $r = $this->input->post();
        $this->db->insert('guru', $r);
        redirect('master/guru');   
    }

    public function editGuru($id)
    {
        $guru = $this->db->get_where('guru', ['id_guru' => $id])->row();
        $this->load->view('header');
        $this->load->view('editguru', ['guru' => $guru]);
        $this->load->view('footer');
    }

    public function updateGuru()
    {
        $r = $this->input->post();
        $this->db->update('guru', $r, ['id_guru' => $r['id_guru']]);
        redirect('master/guru');   
    }

    public function delGuru($id)
    {
        $this->db->delete('guru', ['id_guru' => $id]);
        redirect('master/guru');   
    }

    public function pelajaran()
    {
        $r = $this->db->get('pelajaran')->result_array();
        $this->load->view('header');
        $this->load->view('pelajaran', ['data' => $r]);
        $this->load->view('footer');
    }

    public function addPelajaran()
    {
        $r = $this->input->post();
        $this->db->insert('pelajaran', $r);
        redirect('master/pelajaran');   
    }

    public function editPelajaran($id)
    {
        $r = $this->db->get_where('pelajaran', ['id_pelajaran' => $id])->row();
        $this->load->view('header');
        $this->load->view('editpelajaran', ['data' => $r]);
        $this->load->view('footer');
    }

    public function updatePelajaran()
    {
        $r = $this->input->post();
        $this->db->update('pelajaran', $r, ['id_pelajaran' => $r['id_pelajaran']]);
        redirect('master/pelajaran');   
    }

    public function delPelajaran($id)
    {
        $this->db->delete('pelajaran', ['id_pelajaran' => $id]);
        redirect('master/pelajaran');   
    }

    public function kelas()
    {
        $r = $this->db->get('kelas')->result_array();
        $this->load->view('header');
        $this->load->view('kelas', ['data' => $r]);
        $this->load->view('footer');
    }

    public function addKelas()
    {
        $r = $this->input->post();
        $this->db->insert('kelas', $r);
        redirect('master/kelas');   
    }

    public function editKelas($id)
    {
        $r = $this->db->get_where('kelas', ['id_kelas' => $id])->row();
        $this->load->view('header');
        $this->load->view('editkelas', ['data' => $r]);
        $this->load->view('footer');
    }

    public function updateKelas()
    {
        $r = $this->input->post();
        $this->db->update('kelas', $r, ['id_kelas' => $r['id_kelas']]);
        redirect('master/kelas');   
    }

    public function delKelas($id)
    {
        $this->db->delete('kelas', ['id_kelas' => $id]);
        redirect('master/kelas');   
    }

    public function libur()
    {
        
    }

    public function kelasPelajaran()
    {
        $sql = 'SELECT kp.id_kelas_pelajaran, kp.id_kelas, kp.id_pelajaran, kp.jam_pelajaran, k.kelas, p.pelajaran FROM kelas_pelajaran kp, pelajaran p, kelas k
        WHERE kp.id_kelas = k.id_kelas AND kp.id_pelajaran = p.id_pelajaran';
        
        $r = $this->db->query($sql)->result_array();
        $kelas = $this->db->get('kelas')->result_array();
        $pelajaran = $this->db->get('pelajaran')->result_array();
        $this->load->view('header');
        $this->load->view('kelaspelajaran', ['data' => $r, 'kelas' => $kelas, 'pelajaran' => $pelajaran]);
        $this->load->view('footer');
    }

    public function addKelasPelajaran()
    {
        $r = $this->input->post();
        $this->db->insert('kelas_pelajaran', $r);
        redirect('master/kelasPelajaran');   
    }

    public function editKelasPelajaran($id)
    {
        $sql = 'SELECT kp.id_kelas_pelajaran, kp.id_kelas, kp.id_pelajaran, kp.jam_pelajaran, k.kelas, p.pelajaran FROM kelas_pelajaran kp, pelajaran p, kelas k
        WHERE kp.id_kelas = k.id_kelas AND kp.id_pelajaran = p.id_pelajaran AND kp.id_kelas_pelajaran = ?';
        
        $r = $this->db->query($sql, [$id])->row();
        // print_r($r);
        // die();
        $this->load->view('header');
        $this->load->view('editkelasPelajaran', ['data' => $r]);
        $this->load->view('footer');
    }

    public function updateKelasPelajaran()
    {
        $r = $this->input->post();
        $this->db->update('kelas_pelajaran', $r, ['id_kelas_pelajaran' => $r['id_kelas_pelajaran']]);
        redirect('master/kelasPelajaran');   
    }

    public function delKelasPelajaran($id)
    {
        $this->db->delete('kelas_pelajaran', ['id_kelas_pelajaran' => $id]);
        redirect('master/kelasPelajaran');   
    }

    public function guruPelajaran()
    {
        $sql = 'SELECT gp.id_guru_pelajaran, gp.id_guru, gp.id_pelajaran, gp.jam_mengajar, g.guru, p.pelajaran FROM guru_pelajaran gp, guru g, pelajaran p
        WHERE gp.id_guru = g.id_guru AND gp.id_pelajaran = p.id_pelajaran';
        
        $r = $this->db->query($sql)->result_array();
        $guru = $this->db->get('guru')->result_array();
        $pelajaran = $this->db->get('pelajaran')->result_array();
        $this->load->view('header');
        $this->load->view('gurupelajaran', ['data' => $r, 'guru' => $guru, 'pelajaran' => $pelajaran]);
        $this->load->view('footer');
    }

    public function addGuruPelajaran()
    {
        $r = $this->input->post();
        $this->db->insert('guru_pelajaran', $r);
        redirect('master/guruPelajaran');   
    }

    public function editGuruPelajaran($id)
    {
        $sql = 'SELECT gp.id_guru_pelajaran, gp.id_guru, gp.id_pelajaran, gp.jam_mengajar, g.guru, p.pelajaran FROM guru_pelajaran gp, guru g, pelajaran p
        WHERE gp.id_guru = g.id_guru AND gp.id_pelajaran = p.id_pelajaran AND gp.id_guru_pelajaran = ?';
        
        $r = $this->db->query($sql, [$id])->row();
        $this->load->view('header');
        $this->load->view('editGuruPelajaran', ['data' => $r]);
        $this->load->view('footer');
    }

    public function updateGuruPelajaran()
    {
        $r = $this->input->post();
        $this->db->update('guru_pelajaran', $r, ['id_guru_pelajaran' => $r['id_guru_pelajaran']]);
        redirect('master/guruPelajaran');   
    }

    public function delGuruPelajaran($id)
    {
        $this->db->delete('guru_pelajaran', ['id_guru_pelajaran' => $id]);
        redirect('master/guruPelajaran');   
    }
}