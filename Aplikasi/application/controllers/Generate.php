<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Generate extends CI_Controller {

	function __construct() {
		parent::__construct();
		$this->load->model('model');
		ini_set('max_execution_time', 0); 
		ini_set('memory_limit','2048M');
    }
    
    public function index()
    {
        $data = $this->db->get('hasil')->result_array();
        // print_r($data);
        // die();
        $this->load->view('header');
        $this->load->view('hasil', ['data' => $data]);
        $this->load->view('footer');
    }

	public function jadwal2($id)
	{
		$a = $this->db->get_where('hasil', ['id' => $id])->result()[0]->gen_terbaik;
		$a = json_decode($a);
		$this->load->view('jadwal', ['data'=>$a]);
		
    }
    
	public function generate()
	{
		// redirect('welcome/grafik');
		// redirect('welcome/jadwal');
		// die();
		// print_r($a);
		$skenario = $this->input->post('skenario');
		$banyak_gen = $this->input->post('banyak_gen');
		$max_generasi = $this->input->post('maksimal_generasi');
		$crossover_rate = $this->input->post('crossover_rate');
		$mutasi_rate = $this->input->post('mutasi_rate');
		
		$iterasi = 1;
		// echo $skenario.$iterasi;
		// die();

		// while($iterasi > 0){
			$this->gen_terbaik = [];
			$this->fitness_terbaik = 0;
			$this->tabrak = 1000;
			$this->libur = 1000;
			$this->pelajaran = 1000;
			$this->guru = 1000;

			$time = microtime();
			$time = explode(' ', $time);
			$time = $time[1] + $time[0];
			$start = $time;

			$data_kelas = $this->model->get_kelas_pelajaran();
			$data_guru = $this->model->get_guru_pelajaran();
			$gen = $this->generate_gen($data_kelas, $data_guru);
		
			for ($i=0; $i < $banyak_gen; $i++) { 
				foreach ($gen as $key => $value) {
					shuffle($value['guru']);
					$gen[$key]['guru'] = $value['guru'];
				}
				$arr[$i] = $gen;
            }

			for ($i=0; $i < $banyak_gen; $i++) { 
				$arr[$i] = $this->arr2gen($arr[$i]);
			}

			$banyak_crossover = $crossover_rate * $banyak_gen;
			$banyak_mutasi = 0;

			$generasi = 1;
			while ($generasi <= $max_generasi AND $this->tabrak > 0 ) {
				// Hitung Error untuk setiap gen
				for ($i=0; $i < $banyak_gen; $i++) { 
					$arr[$i] = $this->tabrak($arr[$i]);
					// Ubah format gen menjadi jam pelajaran perhari
					$arr[$i] = $this->gen2jam($arr[$i]);

					$arr[$i] = $this->pelajaran($arr[$i]);
					$arr[$i] = $this->guru($arr[$i]);
					$arr[$i] = $this->libur($arr[$i]);
				}

				// Seleksi
				$arr = $this->seleksi($arr);

				// CrossOver sebanyak Crossover Rate
				for ($i=0; $i < $banyak_crossover; $i++) { 
					$r_cross[$i] = rand(0, $banyak_gen-1);
				}
				for ($i=0; $i <= $banyak_crossover-1 ; $i++) { 
					if ($i == $banyak_crossover-1) {
						$arr[$r_cross[$i]] = $this->crossover($arr[$r_cross[$i]], $arr[$r_cross[0]]);
					} else {
						$arr[$r_cross[$i]] = $this->crossover($arr[$r_cross[$i]], $arr[$r_cross[$i+1]]);
					}
				}

				// Bentuk Kembali jadi arr [Per Pelajaran]
				for ($i=0; $i < $banyak_gen; $i++) {
					$arr[$i] = $this->jam2gen($arr[$i]);
					$arr[$i] = $this->gen2arr($arr[$i]);
				}

				if ($banyak_mutasi === 0) {
					$banyak_mutasi = $mutasi_rate * count($arr) * count($arr[0]) * count($arr[0][0]['guru']);				
				}

				// Mutasi Sebanyak mutasi rate
				for ($i=0; $i < $banyak_mutasi ; $i++) { 
					$arr = $this->mutasi($arr);
				}
				
				for ($i=0; $i < $banyak_gen; $i++) {
					$arr[$i] = $this->arr2gen($arr[$i]);
				}
				// echo '<br>Fitness Terbaik generasi ke '.$generasi.' : '.$this->fitness_terbaik;
				// echo '<br>Tabrak generasi ke '.$generasi.' : '.$this->tabrak;
				// echo json_encode($this->gen_terbaik);
				// $data = [
				// 	'tabrak' => $this->tabrak,
				// 	'libur' => $this->libur,
				// 	'guru' => $this->guru,
				// 	'pelajaran' => $this->pelajaran,
				// 	'fitness' => $this->fitness_terbaik * 10000,
				// 	'generasi_ke' => $generasi,
				// 	'skenario' => $skenario.$iterasi
				// ];
				// $this->db->insert('hasil_per_generasi', $data);
				$generasi++;
				// die();
			}

			$time = microtime();
			$time = explode(' ', $time);
			$time = $time[1] + $time[0];
			$finish = $time;
			$total_time = round(($finish - $start), 5);
			echo "Selesai dalam ".$total_time." detik";
			$data = [
				'banyak_gen' => $banyak_gen,
				'maksimal_generasi' => $max_generasi,
				'crossover_rate' => $crossover_rate * 10,
				'mutasi_rate' => $mutasi_rate * 10,
				'tabrak' => $this->tabrak,
				'libur' => $this->libur,
				'guru' => $this->guru,
				'pelajaran' => $this->pelajaran,
				'fitness' => $this->fitness_terbaik * 10000,
				'gen_terbaik' => json_encode($this->gen_terbaik),
				'skenario' => $skenario,
				'skenario_ke' => $iterasi,
				'waktu_eksekusi' => $total_time,
				'memory' =>memory_get_usage(),
			];
			echo '<br>Fitness Terbaik iterasi ke '.$iterasi.' : '.$this->fitness_terbaik;
			echo '<br>Tabrak Iterasi ke '.$iterasi.' : '.$this->tabrak;
			echo '<br>Guru Iterasi ke '.$iterasi.' : '.$this->guru;
			echo '<br>Pelajaran Iterasi ke '.$iterasi.' : '.$this->pelajaran;
				
			$this->db->insert('hasil', $data);
			$id = $this->db->insert_id();
			redirect('generate/jadwal2/'.$id);

			echo '<br>SELESAI DIE';
			die();

		// 	// $iterasi--;
		// // }
		// echo '<br>SELESAI';

		// die();

		
		// foreach ($arr as $key => $value) {
		// die(print_r($value));
		// 	echo "Gen ke : ".$key."<br>";
		// 	echo "Total Error Tabrak : ".$value[0]['total_err_tabrak']."<br>";
		// 	echo "Total Error Libur : ".$value[0]['total_err_libur']."<br>";
		// 	echo "Total Error Pelajaran : ".$value[0]['total_err_pelajaran']."<br>";
		// 	echo "Total Error Guru : ".$value[0]['total_err_guru']."<br>";
		// 	echo "<br>";
		// }
	}

	function generate_gen($kelas_pelajaran, $guru_pelajaran)
	{
		$kolom = array_column($guru_pelajaran, 'id_pelajaran');
		foreach ($kelas_pelajaran as $k => $kelas) { // Proses Menyusun Guru yang mengajar dalam setiap kelas
			foreach ($kelas['kelas_pelajaran'] as $k2 => $val) { // Untuk setiap pelajaran dalam kelas
				// Cari guru yang mengajar pelajaran
				$cari = array_search($val->id_pelajaran, $kolom);
				$data[$k2]['pelajaran'] = $val->pelajaran;
				$data[$k2]['id_pelajaran'] = $val->id_pelajaran;
				$data[$k2]['jam_pelajaran'] = $val->jam_pelajaran;
				if (!empty($guru_pelajaran[$cari]['guru_pelajaran'][0]) AND 
					$guru_pelajaran[$cari]['guru_pelajaran'][0]->jam_mengajar >= $val->jam_pelajaran)
				{
					$data[$k2]['guru'] = $guru_pelajaran[$cari]['guru_pelajaran'][0]->guru;
					$data[$k2]['id_guru'] = $guru_pelajaran[$cari]['guru_pelajaran'][0]->id_guru;
					$guru_pelajaran[$cari]['guru_pelajaran'][0]->jam_mengajar 
					= $guru_pelajaran[$cari]['guru_pelajaran'][0]->jam_mengajar - $val->jam_pelajaran;
					if ($guru_pelajaran[$cari]['guru_pelajaran'][0]->jam_mengajar == 0) {
						array_shift($guru_pelajaran[$cari]['guru_pelajaran']);
					}
				}
			}
			$kelas_guru[$k]['id_kelas'] = $kelas['id_kelas'];
			$kelas_guru[$k]['kelas'] = $kelas['kelas'];
			$kelas_guru[$k]['guru'] = $data;
		}
		foreach ($kelas_guru as $k => $value) { // Proses membagi jam pelajaran menjadi maksimal 2 jam pelajaran
			$pelajaran = [];
			foreach ($value['guru'] as $k2 => $val) {
				while ($val['jam_pelajaran'] > 0) {
					$data = $val;
					if ($val['jam_pelajaran'] > 2) {
						$data['jam_pelajaran'] = 2;
						$val['jam_pelajaran'] -= 2; 
					} else {
						$data['jam_pelajaran'] = $val['jam_pelajaran'];
						$val['jam_pelajaran'] = 0; 
					}
					array_push($pelajaran, $data);}}
			shuffle($pelajaran);
			$kelas_guru[$k]['guru'] = $pelajaran; 
		}
		return $kelas_guru;
	}

	// Ubah dari jam pelajaran menjadi gen
	function arr2gen($kelas_guru)
	{
		foreach ($kelas_guru as $k => $value) {
			$pelajaran = [];
			foreach ($value['guru'] as $k2 => $val) {
				for ($i=0; $i<$val['jam_pelajaran']; $i++) { 
					array_push($pelajaran, $val);
				}
			}
			$kelas_guru[$k]['guru'] = $pelajaran; 
		}
		return $kelas_guru;
	}

	// Ubah dari gen menjadi jam pelajaran 
	function gen2arr($kelas_guru)
	{
		foreach ($kelas_guru as $k => $value) {
			$pelajaran = [];
			$i = 0;
			while ($i < count($value['guru'])) {
				if ($value['guru'][$i]['jam_pelajaran'] == 2) {
					array_push($pelajaran, $value['guru'][$i]);
					$i += 2;
				} else {
					array_push($pelajaran, $value['guru'][$i]);
					$i ++;
				}
			}
			$kelas_guru[$k]['guru'] = $pelajaran; 
		}
		return $kelas_guru;
	}

	// Ubah dari gen menjadi jam pelajaran perhari
	function gen2jam($kelas_guru)
	{
		$jam = $this->model->get_jam_perhari();
		$hari = ['senin', 'selasa', 'rabu', 'kamis', 'jumat', 'sabtu', 'minggu'];
		foreach ($kelas_guru as $key => $value) {
			$counter = 0;
			foreach ($hari as $k => $val) {
				$data[$val] = [];
				for ($i=0; $i < $jam->$val; $i++) { 
					array_push($data[$val], $value['guru'][$counter]);
					$counter++;
				}
			}
			$kelas_guru[$key]['guru'] = $data;
		}	
		return $kelas_guru;
	}

	// Ubah dari jam pelajaran perhari menjadi gen
	function jam2gen($kelas_guru)
	{
		foreach ($kelas_guru as $key => $value) {
			$data = [];
			foreach ($value['guru'] as $k => $val) {
				if (count($val) > 1) {
					foreach ($val as $k2 => $v) {
						array_push($data, $v);
					}	
				}
			}
			$kelas_guru[$key]['guru'] = $data;
		}	

		return $kelas_guru;
	}

	// Hitung Tabrak Jadwal Guru antar kelas dalam satu gen
	function tabrak($gen)
	{
		$x = 0;
		for ($i=0; $i<count($gen)-1; $i++) { 
			for ($j=$i; $j<count($gen); $j++) {
				if($i != $j){
					foreach ($gen[$i]['guru'] as $key => $value) {
						if (!isset($gen[$i]['guru'][$key]['tabrak'])) {
							$gen[$i]['guru'][$key]['tabrak_detail'] = '';
							$gen[$i]['guru'][$key]['tabrak'] = 0;						
						}	

						if (!isset($gen[$j]['guru'][$key]['tabrak'])) {
							$gen[$j]['guru'][$key]['tabrak_detail'] = '';
							$gen[$j]['guru'][$key]['tabrak'] = 0;						
						}

						if ($gen[$j]['guru'][$key]['id_guru'] == $value['id_guru']) {
							$x++;
							$gen[$i]['guru'][$key]['tabrak']++;
							// $gen[$i]['guru'][$key]['tabrak_detail'] .= '('.$gen[$j]['kelas'].')';
							$gen[$j]['guru'][$key]['tabrak']++;
							// $gen[$j]['guru'][$key]['tabrak_detail'] .= '('.$gen[$i]['kelas'].')';
						}
					}
				}
			}
		}
		$gen[0]['total_err_tabrak'] = $x;
		return $gen;
	}

	// Hitung Guru yang terjadwal ketika libur -> input: gen2jam
	function libur($gen)
	{
		$off = $this->model->get_libur_guru();
		$x = 0;
		foreach ($gen as $g => $kelas) {
			foreach ($off as $key => $value) {
				if ($value['senin'] == 1) {
					$kolom = array_column($kelas['guru']['senin'], 'id_guru');
					$count = array_count_values($kolom);
					if(isset($count[$value['id_guru']]))
						$x += $count[$value['id_guru']];
				}
				if ($value['selasa'] == 1) {
					$kolom = array_column($kelas['guru']['selasa'], 'id_guru');
					$count = array_count_values($kolom);
					if(isset($count[$value['id_guru']]))
						$x += $count[$value['id_guru']];
				}
				if ($value['rabu'] == 1) {
					$kolom = array_column($kelas['guru']['rabu'], 'id_guru');
					$count = array_count_values($kolom);
					if(isset($count[$value['id_guru']]))
						$x += $count[$value['id_guru']];
				}
				if ($value['kamis'] == 1) {
					$kolom = array_column($kelas['guru']['kamis'], 'id_guru');
					$count = array_count_values($kolom);
					if(isset($count[$value['id_guru']]))
						$x += $count[$value['id_guru']];
				}
				if ($value['jumat'] == 1) {
					$kolom = array_column($kelas['guru']['jumat'], 'id_guru');
					$count = array_count_values($kolom);
					if(isset($count[$value['id_guru']]))
						$x += $count[$value['id_guru']];
				}	
			}
		}
		$gen[0]['total_err_libur'] = $x;
		return $gen;
	}

	// Hitung Pelajaran yang terjadwal dalam satu hari -> input: gen2jam
	function pelajaran($gen)
	{
		$x = 0;
		foreach ($gen as $key => $value) {
			$x1 = 0;
			foreach ($value['guru'] as $k => $val) {
				$kolom = array_column($val, 'id_pelajaran');
				$count = array_count_values($kolom);
				$gen[$key]['guru'][$k][0]['err_pelajaran_sehari'] = 0;
				foreach ($count as $c => $v) {
					if ($v > 3) {
						$x += $v;
						$x1 += $v;
						$gen[$key]['guru'][$k][0]['err_pelajaran_sehari'] += $v;
					}
				}
				if ($gen[$key]['guru'][$k][0]['err_pelajaran_sehari'] == 0){
					unset($gen[$key]['guru'][$k][0]['err_pelajaran_sehari']);
				}
			}
			$gen[$key]['err_pelajaran_sekelas'] = $x1;
		}
		$gen[0]['total_err_pelajaran'] = $x;
		return $gen;
	}

	// Hitung Guru yang terjadwal dalam satu hari -> input: gen2jam
	function guru($gen)
	{
		$x = 0;
		foreach ($gen as $key => $value) {
			$x1 = 0;
			foreach ($value['guru'] as $k => $val) {
				$kolom = array_column($val, 'id_guru');
				$count = array_count_values($kolom);
				$gen[$key]['guru'][$k][0]['err_guru_sehari'] = 0;
				foreach ($count as $c => $v) {
					if ($v > 3) {
						$x += $v;
						$x1 += $v;
						$gen[$key]['guru'][$k][0]['err_guru_sehari'] += $v;
					}
				}
				if ($gen[$key]['guru'][$k][0]['err_guru_sehari'] == 0){
					unset($gen[$key]['guru'][$k][0]['err_guru_sehari']);
				}
			}
			$gen[$key]['err_guru_sekelas'] = $x1;
		}
		$gen[0]['total_err_guru'] = $x;
		return $gen;
	}
	// Fungsi untuk seleksi gen, mengembalikan gen terpilih sejumlah sama dengan jumlah gen awal 
	// Input bentuk jam
	function seleksi($arr)
	{
		// 0,9900990099009901
		// 0,999000999000999
		$TF = 0;
		// Hitung nilai fitness
		$fitness = 0;
		foreach ($arr as $key => $value) {
			$arr[$key][0]['fitnes'] = 1 / (1 +
			($value[0]['total_err_tabrak'] * 0.01+
			$value[0]['total_err_libur'] * 0.01 +
			$value[0]['total_err_pelajaran'] * 0.00001 +
			$value[0]['total_err_guru'] * 0.00001 ));
			$TF += $arr[$key][0]['fitnes'];
			if ($arr[$key][0]['fitnes'] > $fitness) {
				$best = $key;
			}
		}

		// Cari gen terbaik
		if ($this->gen_terbaik == []) {
			$this->gen_terbaik = $arr[$best];
			$this->fitness_terbaik = $arr[$best][0]['fitnes'];
			$this->tabrak = $arr[$best][0]['total_err_tabrak'];
			$this->libur = $arr[$best][0]['total_err_libur'];
			$this->pelajaran = $arr[$best][0]['total_err_pelajaran'];
			$this->guru = $arr[$best][0]['total_err_guru'];
		} else {
			if ($this->fitness_terbaik <= $arr[$best][0]['fitnes']) {
				$this->fitness_terbaik = $arr[$best][0]['fitnes'];
				$this->tabrak = $arr[$best][0]['total_err_tabrak'];
				$this->libur = $arr[$best][0]['total_err_libur'];
				$this->pelajaran = $arr[$best][0]['total_err_pelajaran'];
				$this->guru = $arr[$best][0]['total_err_guru'];
				$this->gen_terbaik = $arr[$best];
			}
		}

		// Whell untuk bagian setiap gen
		$wheel = 0;
		foreach ($arr as $key => $value) {
			$arr[$key][0]['probabilitas'] = $arr[$key][0]['fitnes'] / $TF;
			$arr[$key][0]['kode_gen'] = $key;
			$wheel += $arr[$key][0]['probabilitas'];
			$arr[$key][0]['wheel'] = $wheel;
		}
		// RorretWhell sejumlah gen
		for ($j=0; $j<count($arr); $j++) { 
			$r = rand(1, 100)/100;
			$x = 0;
			for ($i=1; $i<count($arr); $i++) { 
				if ($r <= $arr[$i][0]['wheel'] AND $r > $arr[$i-1][0]['wheel']) {
					$x = $i;
				}
			}	
			$new_arr[$j] = $arr[$x];
		}
		// Elitism
		$new_arr[rand(0, count($new_arr)-1)] = $arr[$best];
		return $new_arr;		
	}

	// Mengembalikan gen baru hasil crossover dua induk, Bentuk Jam 
	// Dipotong sesuai kelas, satu kelas ikut sekaligus
	function crossover($gen1, $gen2)
	{
		$count = count($gen1);
		
		for ($i=0; $i < $count; $i++) { 
			$rand = rand(0, 100); 			
			if ($rand > 50) {
				$new[$i] = $gen1[$i];
			} else {
				$new[$i] = $gen2[$i];
			}
		}
		return $new;
	}

	// Mutasi sekali dari seluruh populasi, bentuk Arr
	function mutasi($populasi)
	{
		$pop = count($populasi);
		$kelas = count($populasi[0]);
		$pelajaran = count($populasi[0][0]['guru']);
		$tot = $pop * $kelas * $pelajaran;

		// $ulang = true;
		// while ($ulang) {
			$rand = rand(0, $tot-1);
			$genKe = intval($rand/($pelajaran*$kelas));
			$sisaGen = $rand % ($pelajaran*$kelas);
			$kelasKe = intval($sisaGen / $pelajaran);
			$pelajaranKe = $sisaGen % $pelajaran;

			// if($populasi[$genKe][$kelasKe]['guru'][$pelajaranKe]['jam_pelajaran'] == 2){
			// 	$ulang = false;
			// }
		// }

		$sudahTukar = false;
		while(!$sudahTukar){
			$tukar = rand(0, $pelajaran-1);
			if ($populasi[$genKe][$kelasKe]['guru'][$pelajaranKe]['jam_pelajaran'] 
					== $populasi[$genKe][$kelasKe]['guru'][$tukar]['jam_pelajaran']) 
			{
				$temp = $populasi[$genKe][$kelasKe]['guru'][$pelajaranKe];
				$populasi[$genKe][$kelasKe]['guru'][$pelajaranKe] = $populasi[$genKe][$kelasKe]['guru'][$tukar];
				$populasi[$genKe][$kelasKe]['guru'][$tukar] = $temp;
				$sudahTukar = true;
			}
		}

		return $populasi;
	}
}
