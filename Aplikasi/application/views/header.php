<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="x-ua-compatible" content="ie=edge">

  <title>Penjadwalan</title>

  <!-- Font Awesome Icons -->
  <link rel="stylesheet" href="<?=base_url('assets/')?>plugins/fontawesome-free/css/all.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?=base_url('assets/')?>dist/css/adminlte.min.css">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
</head>
<body class="hold-transition sidebar-mini">
<div class="wrapper">

  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand navbar-white navbar-light">
  </nav>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="index3.html" class="brand-link">
      <img src="<?=base_url('assets/logo.png')?>" alt="Logo UIN Malang" class="brand-image img-circle elevation-3"
           style="opacity: .8">
      <span class="brand-text font-weight-light">Penjadwalan</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          <li class="nav-item">
            <a href="<?=base_url('/master/guru')?>" class="nav-link">
              <i class="nav-icon fas fa-th"></i>
              <p>
                Master Guru
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="<?=base_url('/master/pelajaran')?>" class="nav-link">
              <i class="nav-icon fas fa-th"></i>
              <p>
                Master Pelajaran
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="<?=base_url('/master/kelas')?>" class="nav-link">
              <i class="nav-icon fas fa-th"></i>
              <p>
                Master Kelas
              </p>
            </a>
          </li>
          <!-- <li class="nav-item">
            <a href="<?=base_url('/master/libur')?>" class="nav-link">
              <i class="nav-icon fas fa-th"></i>
              <p>
                Master Libur Guru
              </p>
            </a>
          </li> -->
          <li class="nav-item">
            <a href="<?=base_url('/master/kelasPelajaran')?>" class="nav-link">
              <i class="nav-icon fas fa-th"></i>
              <p>
                Master Kelas - Pelajaran
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="<?=base_url('/master/guruPelajaran')?>" class="nav-link">
              <i class="nav-icon fas fa-th"></i>
              <p>
                Master Guru - Pelajaran
              </p>
            </a>
          </li>
          <li class="nav-item has-treeview menu-open">
            <a href="<?=base_url('/generate')?>" class="nav-link active">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p>
                Generate Jadwal
              </p>
            </a>
          </li>
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>
