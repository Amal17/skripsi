  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Master Guru - Pelajaran</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Guru Pelajaran</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-6">

        <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Tambah Guru Pelajaran</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form role="form" action="<?=base_url('/master/addGuruPelajaran')?>" method="POST">
                <div class="card-body">
                    <div class="form-group">
                        <label>Guru</label>
                        <select class="form-control" name="id_guru">
                          <?php
                            foreach ($guru as $v) {
                                echo '<option value="'.$v['id_guru'].'">'.$v['guru'].'</option>';
                            }
                          ?>
                          
                        </select>
                    </div>

                    <div class="form-group">
                        <label>Pelajaran</label>
                        <select class="form-control" name="id_pelajaran">
                        <?php
                            foreach ($pelajaran as $v) {
                                echo '<option value="'.$v['id_pelajaran'].'">'.$v['pelajaran'].'</option>';
                            }
                          ?>
                        </select>
                    </div>

                    <div class="form-group">
                    <label for="exampleInputEmail1">Jumlah Jam Mengajar</label>
                    <input type="text" class="form-control" name="jam_mengajar" placeholder="6">
                  </div>

                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Submit</button>
                </div>
              </form>
            </div>
            </div>
          
            <!-- <div class="row"> -->
          <div class="col-12">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">Daftar Guru Pelajaran</h3>

                <div class="card-tools">
                  <div class="input-group input-group-sm" style="width: 100px;">
                    <!-- <input type="text" name="table_search" class="form-control float-right" placeholder="Search"> -->

                    <div class="input-group-append">
                      <!-- <button type="submit" class="btn btn-default"><i class="fas fa-search"></i></button> -->
                    </div>
                  </div>
                </div>
              </div>
              <!-- /.card-header -->
              <div class="card-body table-responsive p-0" style="height: 300px;">
                <table class="table table-head-fixed text-nowrap">
                  <thead>
                    <tr>
                      <th>ID</th>
                      <th>Guru</th>
                      <th>Pelajaran</th>
                      <th>Jam Mengajar</th>
                      <th>Aksi</th>
                      <th></th>
                    </tr>
                  </thead>
                  <tbody>
                      <?php
                      foreach ($data as $v){
                        echo '<tr><td>'.$v['id_guru_pelajaran'].'</td><td>'.$v['guru'].'</td><td>'.$v['pelajaran'].'</td><td>'.$v['jam_mengajar'].'</td>';
                        echo '<td><span class="badge bg-warning"><a href="'.base_url('master/editGuruPelajaran/').$v['id_guru_pelajaran'].'">EDIT</a></span></td>';
                        echo '<td><span class="badge bg-danger"><a href="'.base_url('master/delGuruPelajaran/').$v['id_guru_pelajaran'].'">HAPUS</a></span></td>';
                        echo '</tr>';
                      }
                      ?>
                  </tbody>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
        </div>
            <!-- /.col-md-6 -->
        </div>
        <!-- /.row -->
        
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->