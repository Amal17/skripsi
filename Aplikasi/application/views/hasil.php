  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">GENERATE JADWAL</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Generate</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-6">

        <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Generate Jadwal Baru</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form role="form" action="<?=base_url('/generate/generate')?>" method="POST">
                <div class="card-body">
                  <div class="form-group">
                    <label for="exampleInputEmail1">Skenario</label>
                    <input type="text" class="form-control" name="skenario" placeholder="Contoh">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputPassword1">Banyak Gen</label>
                    <input type="text" class="form-control" name="banyak_gen" placeholder="10">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputPassword1">Maksimal Generasi</label>
                    <input type="text" class="form-control" name="maksimal_generasi" placeholder="1000">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputPassword1">Crossover Rate</label>
                    <input type="text" class="form-control" name="crossover_rate" placeholder="1 - 10">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputPassword1">Mutasi Rate</label>
                    <input type="text" class="form-control" name="mutasi_rate" placeholder="1 - 10">
                  </div>
                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Submit</button>
                </div>
              </form>
            </div>
            </div>
          
            <!-- <div class="row"> -->
          <div class="col-12">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">Daftar Guru</h3>

                <div class="card-tools">
                  <div class="input-group input-group-sm" style="width: 100px;">
                    <!-- <input type="text" name="table_search" class="form-control float-right" placeholder="Search"> -->

                    <div class="input-group-append">
                      <!-- <button type="submit" class="btn btn-default"><i class="fas fa-search"></i></button> -->
                    </div>
                  </div>
                </div>
              </div>
              <!-- /.card-header -->
              <div class="card-body table-responsive p-0" style="height: 300px;">
                <table class="table table-head-fixed text-nowrap">
                  <thead>
                    <tr>
                      <th>ID</th>
                      <th>Banyak Gen</th>
                      <th>Maksimal Generasi</th>
                      <th>Crossover Rate</th>
                      <th>Mutasi Rate</th>
                      <th>Tabrak</th>
                      <th>Guru</th>
                      <th>Pelajaran</th>
                      <th>Fitness</th>
                      <th>Skenario</th>
                      <th>Lama Komputasi</th>
                      <th>Penggunaan Memori</th>
                      <th>Lihat Jadwal</th>
                    </tr>
                  </thead>
                  <tbody>
                      <?php
                      foreach ($data as $v){
                        echo '<tr><td>'.$v['id'].'</td><td>'.$v['banyak_gen'].'</td><td>'.$v['maksimal_generasi'].'</td><td>'.($v['crossover_rate']/100).'</td><td>'.($v['mutasi_rate']/100).'</td><td>'.$v['tabrak'].'</td><td>'.$v['guru'].'</td><td>'.$v['pelajaran'].'</td><td>'.($v['fitness']/10000).'</td><td>'.$v['skenario'].'</td><td>'.$v['waktu_eksekusi'].'</td><td>'.$v['memory'].'</td>';
                        echo '<td><span class="badge bg-warning"><a href="'.base_url('generate/jadwal2/').$v['id'].'">Lihat</a></span></td>';
                        echo '</tr>';
                      }
                      ?>
                  </tbody>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
        </div>
            <!-- /.col-md-6 -->
        </div>
        <!-- /.row -->
        
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->