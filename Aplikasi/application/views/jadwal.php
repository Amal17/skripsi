<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Jadwal</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
</head>
<body>
    <?php //print_r($data); ?>
    <!-- <table> -->
    <div class="table-responsive-sm">
    <table class="table table-sm table-bordered">
    <thead>
        <tr>
        <th scope="row">Kelas</th>
        
    <?php 
        $x = count($data[0]->guru->senin);
        for ($i=1; $i < $x+1 ; $i++) { 
                echo '<th scope="row">Senin '.$i.'</th>';
        }
        $x = count($data[0]->guru->selasa);
        for ($i=1; $i < $x+1 ; $i++) { 
                echo '<th scope="row">Selasa '.$i.'</th>';
        }
        $x = count($data[0]->guru->rabu);
        for ($i=1; $i < $x+1 ; $i++) { 
                echo '<th scope="row">Rabu '.$i.'</th>';
        }
        $x = count($data[0]->guru->kamis);
        for ($i=1; $i < $x+1 ; $i++) { 
                echo '<th scope="row">Kamis '.$i.'</th>';
        }
        $x = count($data[0]->guru->jumat);
        for ($i=1; $i < $x+1 ; $i++) { 
                echo '<th scope="row">Jumat '.$i.'</th>';
        }
        $x = count($data[0]->guru->sabtu);
        for ($i=1; $i < $x+1 ; $i++) { 
                echo '<th scope="row">Sabtu '.$i.'</th>';
        }
        echo '</tr></thead>';
        foreach ($data as $key => $value) {
            echo "<tr><td width=20>".$value->kelas."</td>";
            foreach ($value->guru as $k => $val) {
                foreach ($val as $k2 => $v) {
                    if(isset($v->pelajaran)):
                        echo "<td>".$v->guru."</td>";
                        // echo "<td>".$v->tabrak_detail."</td>";
                    else :
                        // echo "<td></td>";
                    endif;
                }
            }
            echo "</tr>";
        }
    ?>
    </table>
    </div>
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
</body>
</html>